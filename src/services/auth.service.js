import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://52.87.209.179/api/v1/auth/';


class AuthService {
    login(user) {
        return axios
            .post(API_URL + 'login', {
                rut: user.rut,
                email: user.email,
                password: user.password
            })
            .then(response => {
                if (response.data.user.tipo_user == 'Funcionario') {
                    return this.detail(response.data.user, response.data.token);
                }else{
                    return null;
                }

            });
    }

    detail(user, token) {
        return axios
            .get("http://52.87.209.179/api/buscarFuncionario/" + user.id)
            .then((response) => {
                sessionStorage.setItem('user', JSON.stringify({
                    'token': token,
                    'user': user,
                    'userDetail': response.data[0],
                }));

                return {
                    'token': token,
                    'user': user,
                    'userDetail': response.data[0],
                };
            });
    }

    logout() {

        return axios.get(API_URL + 'logout', {headers: authHeader()})
            .then((response) => {
                if (response.status == 200) {
                    sessionStorage.removeItem('user');
                }
                console.log(response);
            });
    }
}

export default new AuthService();