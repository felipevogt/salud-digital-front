export default [
    {
        path: '',
        name: 'Citas Medicas',
        component: () => import('./pages/Citas.vue')
    },
    {
        path: '/ver_cita',
        name: 'Cita',
        component: () => import('./pages/Ver_Cita.vue')
    },
    {
        path: '/historial_citas',
        name: 'Historial Citas',
        component: () => import('./pages/Historial_Citas.vue')
    },
]